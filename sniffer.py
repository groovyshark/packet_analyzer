import socket
import struct
import formatter as fmt


class Sniffer(object):
    @staticmethod
    def get_mac_addr(data):
        bytes_str = map('{:02x}'.format, data)
        mac_addr = ':'.join(bytes_str).upper()
        
        return mac_addr
    
    @staticmethod
    def ethernet_frame(data):
        dst_mac, src_mac, protocol = struct.unpack('! 6s 6s H', data[:14])
        return Sniffer.get_mac_addr(dst_mac), Sniffer.get_mac_addr(src_mac), socket.htons(protocol), data[14:]
    
    @staticmethod
    def ipv4_packet(data):
        version_header_len = data[0]
        version = version_header_len >> 4
        header_len = (version_header_len & 15) * 4
        
        ttl, protocol, src, dst = struct.unpack('! 8x B B 2x 4s 4s', data[:20])
        
        return version, header_len, ttl, protocol, fmt.ipv4_format(src), fmt.ipv4_format(dst), data[header_len:]
    
    @staticmethod
    def tcp_packet(data):
        (src_port, dst_port, seq, acknowledgement, offset_reserved_flag) = struct.unpack('! H H L L H', data[:14])
        offset = (offset_reserved_flag >> 12) * 4
        flag_urg = (offset_reserved_flag & 32) >> 5
        flag_ack = (offset_reserved_flag & 32) >> 4
        flag_psh = (offset_reserved_flag & 32) >> 3
        flag_rst = (offset_reserved_flag & 32) >> 2
        flag_syn = (offset_reserved_flag & 32) >> 1
        flag_fin = (offset_reserved_flag & 32) >> 1
        
        return src_port, dst_port, seq, acknowledgement, \
               flag_urg, flag_ack, flag_psh, flag_rst, flag_syn, flag_fin, \
               data[offset:]
    
    @staticmethod
    def udp_packet(data):
        src_port, dst_port, size = struct.unpack('! H H 2x H', data[:8])
        return src_port, dst_port, size, data[8:]
    
    @staticmethod
    def icmp_packet(data):
        icmp_type, code, checksum = struct.unpack('! B B H', data[:4])
        return icmp_type, code, checksum, data[4:]
