from sniffer import Sniffer

import formatter as fmt
import socket
import struct


def main():
    conn = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(3))
    
    while True:
        raw_data, addr = conn.recvfrom(65536)
        dst_mac, src_mac, eth_protocol, data = Sniffer.ethernet_frame(raw_data)

        print('\n Ethernet Frame: ')
        print(fmt.TAB_1 + 'Destination: {}, Source: {}, Protocol: {}'.format(dst_mac, src_mac, eth_protocol))
        
        if eth_protocol == 8:
            (version, header_length, ttl, protocol, src, dst, data) = Sniffer.ipv4_packet(data)
            
            print(fmt.TAB_1 + "IPV4 Packet:")
            print(fmt.TAB_2 + 'Version: {}, Header Length: {}, TTL: {}'.format(version, header_length, ttl))
            print(fmt.TAB_3 + 'protocol: {}, Source: {}, Target: {}'.format(protocol, src, dst))
            print(fmt.TAB_2 + 'Packet Size: {}'.format(header_length + len(data)))
            
            # ICMP
            if protocol == 1:
                icmp_type, code, checksum, data = Sniffer.icmp_packet(data)
                
                print(fmt.TAB_1 + 'ICMP Packet:')
                print(fmt.TAB_2 + 'Type: {}, Code: {}, Checksum: {},'.format(icmp_type, code, checksum))
                print(fmt.TAB_2 + 'ICMP Data:')
                print(fmt.format_output_line(fmt.DATA_TAB_3, data))
            
            # TCP
            elif protocol == 6:
                src_port, dst_port, sequence, acknowledgment, flag_urg, flag_ack, flag_psh, flag_rst, flag_syn, flag_fin \
                    = struct.unpack('! H H L L H H H H H H', raw_data[:24])

                print(fmt.TAB_1 + 'TCP Segment:')
                print(fmt.TAB_2 + 'Source Port: {}, Destination Port: {}'.format(src_port, dst_port))
                print(fmt.TAB_2 + 'Sequence: {}, Acknowledgment: {}'.format(sequence, acknowledgment))
                print(fmt.TAB_2 + 'Flags:')
                print(fmt.TAB_3 + 'URG: {}, ACK: {}, PSH: {}'.format(flag_urg, flag_ack, flag_psh))
                print(fmt.TAB_3 + 'RST: {}, SYN: {}, FIN:{}'.format(flag_rst, flag_syn, flag_fin))
                
                if len(data) > 0:
                    print(fmt.TAB_2 + 'TCP Data:')
                    print(fmt.format_output_line(fmt.DATA_TAB_3, data))
            
            # UDP
            elif protocol == 17:
                src_port, dst_port, length, data = Sniffer.udp_packet(data)
                
                print(fmt.TAB_1 + 'UDP Segment:')
                print(fmt.TAB_2 + 'Source Port: {}, Destination Port: {}, Length: {}'.format(src_port, dst_port, length))
            
            else:
                print(fmt.TAB_1 + 'Other IPv4 Data:')
                print(fmt.format_output_line(fmt.DATA_TAB_2, data))


if __name__ == '__main__':
    main()
